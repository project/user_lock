<?php

namespace Drupal\user_lock;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining User lock entities.
 */
interface UserLockConfigEntityInterface extends ConfigEntityInterface {

}
