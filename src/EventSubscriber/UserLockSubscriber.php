<?php

namespace Drupal\user_lock\EventSubscriber;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Checks for user redirection subscriber.
 */
class UserLockSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForRedirection'];
    return $events;
  }

  /**
   * Checks for the redirection.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Request object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function checkForRedirection(RequestEvent $event) {
    if ($event->getRequest()->query->get('id')) {
      $time = date('Y-m-d h:i:s a', time());
      $str_time = strtotime($time);
      $id = $event->getRequest()->query->get('id');
      $entities_load = \Drupal::entityTypeManager()
        ->getStorage('user_lock_config_entity')
        ->loadByProperties(['id' => $id]);
      if ($entities_load) {
        $message = $entities_load[$id]->getLockMessage();
        if ($entities_load[$id]->getLockPeriodFrom()) {
          $default_lock_from = DrupalDateTime::createFromTimestamp($entities_load[$id]->getLockPeriodFrom());
          $lock_from = $default_lock_from->format('Y-m-d h:i:s a');
          $str_lock_from = @strtotime($default_lock_from->format('Y-m-d h:i:s a'));
        }
        if ($entities_load[$id]->getLockPeriodTo()) {
          $default_lock_to = DrupalDateTime::createFromTimestamp($entities_load[$id]->getLockPeriodTo());
          $lock_to = $default_lock_to->format('Y-m-d h:i:s a');
          $str_lock_to = @strtotime($default_lock_to->format('Y-m-d h:i:s a'));
        }
        if (($str_time >= $str_lock_from) && ($str_time <= $str_lock_to)) {
          \Drupal::messenger()->addWarning($message);
          \Drupal::messenger()->addWarning($this->t('You have been locked from @lock_from to @lock_to.', [
            '@lock_from' => $lock_from,
            '@lock_to' => $lock_to,
          ]));
        }
      }
    }
  }

}
